#include "pch.h"
#include "CppUnitTest.h"
#include "../Markov/functionPrototypes.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(ChekingMultiply)
		{
			std::string checker = "11111111111111111111";

			std::string actual = "_1111*11111_";

			Rule rule1 = "_+1 -> _1+";
			Rule rule2 = "1+1 -> 11+";
			Rule rule3 = "1! -> !1";
			Rule rule4 = ",! -> !+";
			Rule rule5 = "_! -> _";
			Rule rule6 = "1*1 -> x,@y";
			Rule rule7 = "1x -> xX";
			Rule rule8 = "X, -> 1,1";
			Rule rule9 = "X1 -> 1X";
			Rule rule10 = "_x -> _X";
			Rule rule11 = ",x -> ,X";
			Rule rule12 = "y1 -> 1y";
			Rule rule13 = "y_ -> _";
			Rule rule14 = "1@1 -> x,@y";
			Rule rule15 = "1@_ -> @_";
			Rule rule16 = ",@_ -> !_";
			Rule rule17 = "++ -> +";
			Rule rule18 = "_1 -> 1";
			Rule rule19 = "1+_ -> 1";
			Rule rule20 = "_+_ -> ";

			std::vector<Rule> rules;

			rules.push_back(rule1);
			rules.push_back(rule2);
			rules.push_back(rule3);
			rules.push_back(rule4);
			rules.push_back(rule5);
			rules.push_back(rule6);
			rules.push_back(rule7);
			rules.push_back(rule8);
			rules.push_back(rule9);
			rules.push_back(rule10);
			rules.push_back(rule11);
			rules.push_back(rule12);
			rules.push_back(rule13);
			rules.push_back(rule14);
			rules.push_back(rule15);
			rules.push_back(rule16);
			rules.push_back(rule17);
			rules.push_back(rule18);
			rules.push_back(rule19);
			rules.push_back(rule20);

			if ((checker == modify(actual, rules)))
			{
				Assert::IsTrue(true);
			}
			else
			{
				Assert::IsTrue(false);
			}
		}

		TEST_METHOD(CheckingMarkovCustom)
		{
			String checker = "11111111111111111111";
			String actual = "_1111*11111_";

			MyStrRule MyStrRule1 = "_+1 -> _1+";
			MyStrRule MyStrRule2 = "1+1 -> 11+";
			MyStrRule MyStrRule3 = "1! -> !1";
			MyStrRule MyStrRule4 = ",! -> !+";
			MyStrRule MyStrRule5 = "_! -> _";
			MyStrRule MyStrRule6 = "1*1 -> x,@y";
			MyStrRule MyStrRule7 = "1x -> xX";
			MyStrRule MyStrRule8 = "X, -> 1,1";
			MyStrRule MyStrRule9 = "X1 -> 1X";
			MyStrRule MyStrRule10 = "_x -> _X";
			MyStrRule MyStrRule11 = ",x -> ,X";
			MyStrRule MyStrRule12 = "y1 -> 1y";
			MyStrRule MyStrRule13 = "y_ -> _";
			MyStrRule MyStrRule14 = "1@1 -> x,@y";
			MyStrRule MyStrRule15 = "1@_ -> @_";
			MyStrRule MyStrRule16 = ",@_ -> !_";
			MyStrRule MyStrRule17 = "++ -> +";
			MyStrRule MyStrRule18 = "_1 -> 1";
			MyStrRule MyStrRule19 = "1+_ -> 1";
			MyStrRule MyStrRule20 = "_+_ -> ";

			std::vector<MyStrRule> MyStrRules;

			MyStrRules.push_back(MyStrRule1);
			MyStrRules.push_back(MyStrRule2);
			MyStrRules.push_back(MyStrRule3);
			MyStrRules.push_back(MyStrRule4);
			MyStrRules.push_back(MyStrRule5);
			MyStrRules.push_back(MyStrRule6);
			MyStrRules.push_back(MyStrRule7);
			MyStrRules.push_back(MyStrRule8);
			MyStrRules.push_back(MyStrRule9);
			MyStrRules.push_back(MyStrRule10);
			MyStrRules.push_back(MyStrRule11);
			MyStrRules.push_back(MyStrRule12);
			MyStrRules.push_back(MyStrRule13);
			MyStrRules.push_back(MyStrRule14);
			MyStrRules.push_back(MyStrRule15);
			MyStrRules.push_back(MyStrRule16);
			MyStrRules.push_back(MyStrRule17);
			MyStrRules.push_back(MyStrRule18);
			MyStrRules.push_back(MyStrRule19);
			MyStrRules.push_back(MyStrRule20);

			if ((checker == custom_modify(actual, MyStrRules)))
			{
				Assert::IsTrue(true);
			}
			else
			{
				Assert::IsTrue(false);
			}
		}
	};
}
